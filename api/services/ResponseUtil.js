/**
 * Created by 0aps on 11/27/16.
 */

module.exports = {
  apiFailure: function (err) {
    sails.log.debug("Error:", err);
    res.send(err);
  }
  
};
