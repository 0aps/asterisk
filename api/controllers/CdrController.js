/**
 * CdrController
 *
 * @description :: Server-side logic for managing cdrs
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  list: function (req, res) {
    Cdr.getListOrQuery(function (results) {
      res.send(results);
    }, "list");
  },

  filter: function (req, res) {
    var filter = req.param('query');
    Cdr.getListOrQuery(function (results) {
      res.send(results);
    }, "query", filter);
  }
};
