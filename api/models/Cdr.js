/**
* Cdr.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  schema: true,
  tableName: 'cdr',
  autoPK:false,
  autoCreatedAt: false,
  autoUpdatedAt: false,
  attributes: {
    'calldate':      'date',
    'clid':          'string',
    'src':           'string',
    'dst':           'string',
    'dcontext':      'string',
    'channel':       'string',
    'dstchannel':    'string',
    'lastapp':       'string',
    'lastdata':      'string',
    'duration':      'int',
    'billsec':       'int',
    'disposition':   'string',
    'amaflags':      'int',
    'accountcode':   'string',
    'uniqueid':      'string',
    'userfield':     'string',
    'did':           'string',
    'recordingfile': 'string',
    'cnum':          'string',
    'cnam':          'string',
    'outbound_cnum': 'string',
    'outbound_cnam': 'string',
    'dst_cnam':      'string'
  },

  getListOrQuery: function (cb, type, query) {
    function success(err, results) {
        if(!results) results = err;
        results = results.map(function (elem) {
          var humanDate = elem.calldate.toLocaleString();
          elem.calldate = humanDate;
          return elem;
        });

        sails.log.info("# of register: ", results.length);
        cb(results);
    };

    if(type === "list"){
      Cdr.find().then(success)
         .catch(ResponseUtil.apiFailure);
    }else{
      Cdr.query(query, success);
    }
  }

};
