/**
 * Created by 0aps on 11/26/16.
 */


(function () {
  'use strict';

  var app = angular.module('asterisk_app', ['chart.js']);
            app.factory("cdr", CDRFactory);
            app.controller("DashboardController", DashboardController);

  function CDRFactory($http){
    return {
      getData: getData,
      getChartData: getChartData
    };

    function getData(query){
       var querySet;
       var endpoint = "cdrs/list";

       if(!query){
         querySet = $http.get(endpoint);
       }else{
         querySet = $http.post(endpoint, query);
       }

       return querySet.then(success)
                      .catch(fail);

      function success(result) {
        return result.data;
      };

      function fail(err) {
        console.error(err);
      };
    };

    function getChartData(cb, query, resource) {
      var allCallPerResourceXDay = "select calldate, src, count(*) as num from cdr where src = %s "+
        "group by date(calldate), src; ";
      var allCallPerResourcesDay = "select date(calldate) as calldate, src, count(*) as num from cdr where src != '' group by date(calldate), src;";
      var allCallPerDay = "select calldate, sum(src) as scr from cdr group by date(calldate);";

      var mapQuery = {
        1: allCallPerResourceXDay,
        2: allCallPerResourceXDay,
        3: allCallPerResourcesDay,
        4: allCallPerDay
      };

      var data = { 'query' : "" };
      data['query'] = mapQuery[query];
      if(resource) data['query'] = data['query'].replace("%s", resource);

      getData(data).then(function (result) {
        var labels = result.map(function (elem) {
          return elem.calldate;
        });
        var labels = removeDuplicates(labels)

        var srcSet = result.map(function(elem){
          return elem.src;
        });
        srcSet = removeDuplicates(srcSet);

        var series = srcSet.map(function(elem){
          return "Employee: %s".replace("%s", elem);
        });

        var data = [];
        for(var i = 0; i < srcSet.length; ++i){
           var rowData = result.filter(function (elem) {
              return elem.src === srcSet[i];
           }).map(function (elem) {
              return elem.num;
           });
           data.push(rowData);
        }

        var result = {
          'labels': labels,
          'series': series,
          'data': data
        };
        cb(result);
      });
    }

    function removeDuplicates(arr){
      var set = new Set(arr);
      return Array.from(set);
    }

  };

  function DashboardController($scope, cdr){
    var vm = $scope;
    vm.cdr_entries = [];
    vm.chart_options = {
      "Extension 1 -- Numero de llamadas por dia": 1,
      "Extension 2 -- Numero de llamadas por dia": 2,
      "Extension 1/2 -- Numero de llamadas por dia": 3,
      "Todas las llamadas realizadas por dia": 4
    };

    cdr.getData().then(function (result) {
      vm.cdr_entries = result;
    });

    vm.changeSelectedChart = function () {
      if(vm.selectedChart === 1){
        getResourceData(1);
      }
      else if(vm.selectedChart === 2){
        getResourceData(2);
      }
      else if(vm.selectedChart === 3){
        getResourceData();
      }

    };

    function getResourceData(resource){
      cdr.getChartData(function (result) {
        vm.labels = result.labels;
        vm.series = result.series;
        vm.data = result.data;
      }, vm.selectedChart, resource);
    };

  };

})();
